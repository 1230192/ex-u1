package com.example.cesareduardo.fechas_practica1;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.view.View.OnClickListener;

import java.util.Calendar;


public class MainActivity extends ActionBarActivity {

    TextView mitv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button miboton = (Button) findViewById(R.id.button);
        miboton.setOnClickListener(new OnClickListener(){
        public void onClick(View v){
            Calendar cal1 = Calendar.getInstance();
            Calendar cal2 = Calendar.getInstance();

            int d = cal2.get(Calendar.DAY_OF_MONTH);
            int m = cal2.get(Calendar.MONTH);
            int y = cal2.get(Calendar.YEAR);

            cal1.set(2015, 1, 1);
            cal2.set(y, m, d);

            long milis1 = cal1.getTimeInMillis();
            long milis2 = cal2.getTimeInMillis();

            long dif = milis2 - milis1;

            String resultado = Long.toString(dif);

            mitv.setText("Han pasado " + resultado + " milisegundos desde el 1 de enero del 2015 a hoy");
            }

        });
         mitv = (TextView) findViewById(R.id.label);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
